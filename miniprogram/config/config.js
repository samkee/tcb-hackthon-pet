//小程序配置
const APPCONFIG = {
  VERSION: 101,
  VERSIONTEXT:'1.0.1'
}

//云开发环境配置
const CLOUDCONFIG = {
	CLOUDENV:'pet-real',//云开发环境，留空默认使用第一个创建的环境
}

//地图相关配置
const MAPCONFIG = {
  MAPKEY: '7WCBZ-TZNLF-XLKJU-NUXQN-HL642-CFB3A',//使用在腾讯位置服务申请的key
  DEFLOCATION: { name: "", latitude: 30.265551, longitude: 120.153603, city: "杭州市", address: "" }//默认坐标

}

module.exports = {
  APPCONFIG:APPCONFIG,
	CLOUDCONFIG:CLOUDCONFIG,
	MAPCONFIG: MAPCONFIG
}